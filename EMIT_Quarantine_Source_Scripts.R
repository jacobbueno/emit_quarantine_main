# Title: EMIT_Quarantine_Source_Scripts.R
# Author: Jacob Bueno de Mesquita
# Date: January 15, 2018; February 14, 2019
# Objective: Run each of the 5 scripts required to reproduce the Main Q analysis and create markdown reports for each of them. Also read the markdown file for the analysis. 

library(markdown)
library(rmarkdown)
library(htmlTable)

source("/Users/jbueno/Desktop/Dissertation_git/EMIT_QUARANTINE_MAIN/EMIT_Quarantine_Main_Cleaning.R")
render("/Users/jbueno/Desktop/Dissertation_git/EMIT_QUARANTINE_MAIN/EMIT_Quarantine_Main_Cleaning.R", "word_document")

source("/Users/jbueno/Desktop/Dissertation_git/EMIT_QUARANTINE_MAIN/EMIT_Quarantine_Main_work_with_clean_files.R")
render("/Users/jbueno/Desktop/Dissertation_git/EMIT_QUARANTINE_MAIN/EMIT_Quarantine_Main_work_with_clean_files.R", "word_document")

source("/Users/jbueno/Desktop/Dissertation_git/EMIT_QUARANTINE_MAIN/EMIT_Quarantine_Main_Analysis.R")
render("/Users/jbueno/Desktop/Dissertation_git/EMIT_QUARANTINE_MAIN/EMIT_Quarantine_Main_Analysis.R", "html_document")

source("/Users/jbueno/Desktop/Dissertation_git/EMIT_QUARANTINE_MAIN/EMIT_Quarantine_Text_Analysis.R")
render("/Users/jbueno/Desktop/Dissertation_git/EMIT_QUARANTINE_MAIN/EMIT_Quarantine_Text_Analysis.R", "html_document")

source("/Users/jbueno/Desktop/Dissertation_git/EMIT_QUARANTINE_MAIN/EMIT_Quarantine_POC_Inf_Criteria.R")
render("/Users/jbueno/Desktop/Dissertation_git/EMIT_QUARANTINE_MAIN/EMIT_Quarantine_POC_Inf_Criteria.R", "html_document")

# source("/Users/jbueno/Desktop/Dissertation_git/EMIT_QUARANTINE_MAIN/Quarantine Main Paper Tables Figures.Rmd")
# "sourcing" the Rmd file didn't work because the working directory for the RMarkdown file sits in a different working directory from that of the input files.
# Thus, after running this script, one must open the Rmd file and select the knit option for html file in order to produce the final report. 